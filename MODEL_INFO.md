
Model info for edgenext_xx_small.in1k
=====================================


Sequential (Input shape: 64 x 3 x 224 x 224)
============================================================================
Layer (type)         Output Shape         Param #    Trainable 
============================================================================
                     64 x 24 x 56 x 56   
Conv2d                                    1176       True      
LayerNorm2d                               48         True      
Identity                                                       
Conv2d                                    240        True      
LayerNorm                                 48         True      
____________________________________________________________________________
                     64 x 56 x 56 x 96   
Linear                                    2400       True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 56 x 56 x 24   
Linear                                    2328       True      
Dropout                                                        
Identity                                                       
Conv2d                                    240        True      
LayerNorm                                 48         True      
____________________________________________________________________________
                     64 x 56 x 56 x 96   
Linear                                    2400       True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 56 x 56 x 24   
Linear                                    2328       True      
Dropout                                                        
Identity                                                       
LayerNorm2d                               48         True      
____________________________________________________________________________
                     64 x 48 x 28 x 28   
Conv2d                                    4656       True      
Conv2d                                    1248       True      
LayerNorm                                 96         True      
____________________________________________________________________________
                     64 x 28 x 28 x 192  
Linear                                    9408       True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 28 x 28 x 48   
Linear                                    9264       True      
Dropout                                                        
Identity                                                       
Conv2d                                    240        True      
____________________________________________________________________________
                     64 x 48 x 28 x 28   
Conv2d                                    3120       True      
LayerNorm                                 96         True      
____________________________________________________________________________
                     64 x 784 x 144      
Linear                                    7056       True      
Dropout                                                        
Linear                                    2352       True      
Dropout                                                        
LayerNorm                                 96         True      
____________________________________________________________________________
                     64 x 28 x 28 x 192  
Linear                                    9408       True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 28 x 28 x 48   
Linear                                    9264       True      
Dropout                                                        
Identity                                                       
LayerNorm2d                               96         True      
____________________________________________________________________________
                     64 x 88 x 14 x 14   
Conv2d                                    16984      True      
Conv2d                                    4400       True      
LayerNorm                                 176        True      
____________________________________________________________________________
                     64 x 14 x 14 x 352  
Linear                                    31328      True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 14 x 14 x 88   
Linear                                    31064      True      
Dropout                                                        
Identity                                                       
Conv2d                                    4400       True      
LayerNorm                                 176        True      
____________________________________________________________________________
                     64 x 14 x 14 x 352  
Linear                                    31328      True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 14 x 14 x 88   
Linear                                    31064      True      
Dropout                                                        
Identity                                                       
Conv2d                                    4400       True      
LayerNorm                                 176        True      
____________________________________________________________________________
                     64 x 14 x 14 x 352  
Linear                                    31328      True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 14 x 14 x 88   
Linear                                    31064      True      
Dropout                                                        
Identity                                                       
Conv2d                                    4400       True      
LayerNorm                                 176        True      
____________________________________________________________________________
                     64 x 14 x 14 x 352  
Linear                                    31328      True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 14 x 14 x 88   
Linear                                    31064      True      
Dropout                                                        
Identity                                                       
Conv2d                                    4400       True      
LayerNorm                                 176        True      
____________________________________________________________________________
                     64 x 14 x 14 x 352  
Linear                                    31328      True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 14 x 14 x 88   
Linear                                    31064      True      
Dropout                                                        
Identity                                                       
Conv2d                                    300        True      
Conv2d                                    300        True      
LayerNorm                                 176        True      
____________________________________________________________________________
                     64 x 196 x 264      
Linear                                    23496      True      
Dropout                                                        
Linear                                    7832       True      
Dropout                                                        
LayerNorm                                 176        True      
____________________________________________________________________________
                     64 x 14 x 14 x 352  
Linear                                    31328      True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 14 x 14 x 88   
Linear                                    31064      True      
Dropout                                                        
Identity                                                       
LayerNorm2d                               176        True      
____________________________________________________________________________
                     64 x 168 x 7 x 7    
Conv2d                                    59304      True      
Conv2d                                    13776      True      
LayerNorm                                 336        True      
____________________________________________________________________________
                     64 x 7 x 7 x 672    
Linear                                    113568     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 7 x 7 x 168    
Linear                                    113064     True      
Dropout                                                        
Identity                                                       
Conv2d                                    420        True      
Conv2d                                    420        True      
Conv2d                                    420        True      
LayerNorm                                 336        True      
____________________________________________________________________________
                     64 x 49 x 504       
Linear                                    85176      True      
Dropout                                                        
Linear                                    28392      True      
Dropout                                                        
LayerNorm                                 336        True      
____________________________________________________________________________
                     64 x 7 x 7 x 672    
Linear                                    113568     True      
GELU                                                           
Dropout                                                        
Identity                                                       
____________________________________________________________________________
                     64 x 7 x 7 x 168    
Linear                                    113064     True      
Dropout                                                        
Identity                                                       
Identity                                                       
____________________________________________________________________________
                     64 x 168 x 1 x 1    
AdaptiveAvgPool2d                                              
AdaptiveMaxPool2d                                              
____________________________________________________________________________
                     64 x 336            
Flatten                                                        
BatchNorm1d                               672        True      
Dropout                                                        
____________________________________________________________________________
                     64 x 512            
Linear                                    172032     True      
ReLU                                                           
BatchNorm1d                               1024       True      
Dropout                                                        
____________________________________________________________________________
                     64 x 13             
Linear                                    6656       True      
____________________________________________________________________________

Total params: 1,336,940
Total trainable params: 1,336,940
Total non-trainable params: 0

Optimizer used: <function Adam at 0x7f00786abf40>
Loss function: FlattenedLoss of CrossEntropyLoss()

Callbacks:
  - TrainEvalCallback
  - CastToTensor
  - Recorder
  - ProgressCallback